
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd

import keras
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers.merge import concatenate
from keras.layers import Dense, Input, Flatten, Dropout, Add, Concatenate
from keras.layers import Conv1D, MaxPooling1D, Embedding, GlobalMaxPooling1D
from keras.layers import LSTM, Bidirectional
from keras.layers import Activation, BatchNormalization, SpatialDropout1D, GaussianNoise
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping
import gensim
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess
from gensim.models.keyedvectors import KeyedVectors

import os

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical


# In[2]:


true_path=os.listdir('./uni-mich-data/Transcription/Truthful/')
false_path=os.listdir('./uni-mich-data/Transcription/Deceptive/')

train_data=[]
for i in true_path:
    i = './uni-mich-data/Transcription/Truthful/'+i
    #print(i)
    data = pd.read_csv(i)
    train_data.append(data)

for j in false_path:
    j = './uni-mich-data/Transcription/Deceptive/'+j
    data = pd.read_csv(j)
    train_data.append(data)

train_data=np.array(train_data)
#print(train_data.shape)
num_samples= len(train_data)
#print(num_samples)
df = pd.DataFrame({'Data':train_data[:]})
df['Data']=df['Data'].astype('str')
#print(df)
#print(df.shape)
labels = np.ones((num_samples), dtype=int)
labels[0:59]=1
labels[60:121]=0
df_labels = pd.DataFrame({'Label':labels[:]})
#print (df_labels)
df['Label']=df_labels
print(df)


# In[3]:


val_data=df.sample(frac=0.2,random_state=200)
train_data=df.drop(val_data.index)


# In[4]:


text=train_data.Data
print(text)


# In[5]:


NUM_WORDS=20000
tokenizer=Tokenizer(num_words=NUM_WORDS,filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n\'')
tokenizer.fit_on_texts(text)
sequences_train=tokenizer.texts_to_sequences(text)
sequences_valid=tokenizer.texts_to_sequences(val_data.Data)
word_index=tokenizer.word_index
print('Found %s unique tokens.' %len(word_index))


# In[6]:


X_train=pad_sequences(sequences_train)
X_val=pad_sequences(sequences_valid,maxlen=X_train.shape[1])
y_train=to_categorical(np.asarray(train_data.Label))
y_val=to_categorical(np.asarray(val_data.Label))
print('Shape of X train and X validation tensor:', X_train.shape, X_val.shape)
print('Shape of label train and validation tensor:', y_train.shape,y_val.shape)


# In[7]:


word_vectors = KeyedVectors.load_word2vec_format('./Word2Vec/GoogleNews-vectors-negative300.bin', binary=True)

EMBEDDING_DIM=300
vocabulary_size=min(len(word_index)+1, NUM_WORDS)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
for word, i in word_index.items():
    if i>=NUM_WORDS:
        continue
    try:
        embedding_vector=word_vectors[word]
        embedding_matrix[i]=embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
        
del(word_vectors)

from keras.layers import Embedding
embedding_layer = Embedding(vocabulary_size,
                           EMBEDDING_DIM,
                           weights=[embedding_matrix],
                           trainable=True)

print(embedding_vector.shape)


# In[8]:


conv_filters = 128 
weight_vec = list(np.max(np.sum(y_train, axis=0))/np.sum(y_train, axis=0))
class_weight = {i: weight_vec[i] for i in range (2)}
print(weight_vec)
print(class_weight)


# In[10]:



inp = Input(shape=(X_train.shape[1],), dtype='int64')
emb = Embedding(len(word_index)+1, EMBEDDING_DIM, weights=[embedding_matrix])(inp)

#Specify each convolution layer and their kernel size i.e. n-grams
conv1_1 = Conv1D(filters=conv_filters, kernel_size=3)(emb)
btch1_1 = BatchNormalization()(conv1_1)
drp1_1 = Dropout(0.2)(btch1_1)
actv1_1 = Activation('relu')(drp1_1)
glmp1_1 = GlobalMaxPooling1D()(actv1_1)

conv1_2 = Conv1D(filters=conv_filters, kernel_size=4)(emb)
btch1_2 = BatchNormalization()(conv1_2)
drp1_2 = Dropout(0.2)(btch1_2)
actv1_2 = Activation('relu')(drp1_2)
glmp1_2 = GlobalMaxPooling1D()(actv1_2)

conv1_3 = Conv1D(filters=conv_filters, kernel_size=5)(emb)
btch1_3 = BatchNormalization()(conv1_3)
drp1_3 = Dropout(0.2)(btch1_3)
actv1_3 = Activation('relu')(drp1_3)
glmp1_3 = GlobalMaxPooling1D()(actv1_3)

conv1_4 = Conv1D(filters=conv_filters, kernel_size=6)(emb)
btch1_4 = BatchNormalization()(conv1_4)
drp1_4 = Dropout(0.2)(btch1_4)
actv1_4 = Activation('relu')(drp1_4)
glmp1_4 = GlobalMaxPooling1D()(actv1_4)

# Gather all convolution layers
cnct = concatenate([glmp1_1, glmp1_2, glmp1_3, glmp1_4], axis =1)
drp1 = Dropout(0.2)(cnct)

#dns1 = Dense(32, activation='relu')(drp1)
dns1 = Dense(300, activation='relu')(drp1)
btch1 = BatchNormalization()(dns1)
drp2 = Dropout(0.2)(btch1)

print(y_train.shape[1])
out = Dense(y_train.shape[1], activation='sigmoid')(drp2)


# In[36]:


model = Model (inputs=inp, outputs=out)
adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])


# In[37]:


model.fit(X_train, y_train, validation_split=0.1, epochs=15, batch_size=32, shuffle=True,
         class_weight=class_weight)


# In[38]:


preds = model.predict(X_val)


# In[39]:


print(preds)


# In[40]:

model.save('word2vec_model.h5')

def myfunc(x):
    index_max = np.argmax(x)
    if index_max==0:
        clas='truthful'
    else:
        if index_max==1:
            clas='deceptive'
    return clas

print(np.apply_along_axis(myfunc, axis=1, arr=preds))
print(np.apply_along_axis(myfunc, axis=1, arr=y_val))

