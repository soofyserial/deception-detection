
# coding: utf-8

# In[1]:


from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution3D, MaxPooling3D,Conv3D

from keras.optimizers import SGD, RMSprop
from keras.utils import np_utils, generic_utils

import theano
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import cv2

from sklearn.cross_validation import train_test_split
from sklearn import cross_validation
from sklearn import preprocessing


# In[19]:
print(cv2.__version__)

# Image Spec
img_rows,img_cols,img_depth=200,200,20

# Training Data
X_tr=[]
X_tr_true=[]
X_tr_false=[]
#True/False Classes
truelisting=os.listdir('./uni-mich-data/Clips/Truthful/')
lielisting=os.listdir('./uni-mich-data/Clips/Deceptive/')

for vid in truelisting:
    vid = "./uni-mich-data/Clips/Truthful/"+vid
    print(vid)
    frames=[]
    cap = cv2.VideoCapture(vid)
    fps = cap.get(5)
    print ("Frames per seconds using video.get(cv2.cv.CV_CAP_PROP_FPS):{0}".format(fps))
    
    for k in range(20):
        ret, frame=cap.read()
    if frame is not None:
        frame=cv2.resize(frame,(img_rows,img_cols),interpolation=cv2.INTER_AREA)
        gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        frames.append(gray)
        
        #plt.imshow(gray,cmap=plt.get_cmap('gray'))
        #plt.xticks([]),plt.yticks([])
        #plt.show()
        #cv2.imshow('frame',gray)
    
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        cap.release()
        cv2.destroyAllWindows()
    
        input=np.array(frames)
        print (input.shape)
        ipt=np.rollaxis(np.rollaxis(input,2,0),2,0)
        print(ipt.shape)
    
        X_tr.append(ipt)
        X_tr_true.append(ipt)
    
for vid in lielisting:
    vid = "./uni-mich-data/Clips/Deceptive/"+vid
    frames=[]
    cap = cv2.VideoCapture(vid)
    fps = cap.get(5)
    print ("Frames per seconds using video.get(cv2.cv.CV_CAP_PROP_FPS):{0}".format(fps))
    
    for k in range(20):
        ret, frame=cap.read()
        if frame is not None:
            frame=cv2.resize(frame,(img_rows,img_cols),interpolation=cv2.INTER_AREA)
            gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            frames.append(gray)
        
        #plt.imshow(gray,cmap=plt.get_cmap('gray'))
        #plt.xticks([]),plt.yticks([])
        #plt.show()
        #cv2.imshow('frame',gray)
    
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            cap.release()
            cv2.destroyAllWindows()
            input=np.array(frames)
            print (input.shape)
            ipt=np.rollaxis(np.rollaxis(input,2,0),2,0)
            print(ipt.shape)
    
            X_tr.append(ipt)
            X_tr_false.append(ipt)

X_tr_true_array = np.array(X_tr_true)
num_truesamples = len(X_tr_true_array)
print ('number of true samples',num_truesamples)


# In[21]:


X_tr_false_array = np.array(X_tr_false)
num_falsesamples = len(X_tr_false_array)
print ('number of false samples', num_falsesamples)


# In[22]:


X_tr_array = np.array(X_tr)
num_samples=len(X_tr_array)
print (num_samples)


# In[23]:


#Assign label to each class
label = np.ones((num_samples),dtype=int)
label[0:59]=1
label[60:121]=0

train_data=[X_tr_array,label]
(X_train, y_train)=(train_data[0],train_data[1])
print('X_train shape:', X_train.shape)
print('y_train shape:',y_train.shape)


# In[24]:


train_set = np.zeros((num_samples,1,img_rows, img_cols, img_depth))
for h in range(num_samples):
    train_set[h][0][:][:][:]=X_train[h,:,:,:]

patch_size=20 #img depth or number of frames used for each video

print(train_set.shape, 'train samples')


# In[25]:


# CNN training parameters
batch_size=2
nb_classes=2
nb_epoch=1

#convert class vectors to binary class matrices
Y_train=np_utils.to_categorical(y_train,nb_classes)

#number of convolutional filters to use at each layer
nb_filters=[32,32]

#level of pooling to perform at each layer
nb_pool=[3,3]

#level of convolution to perform at each layer 
nb_conv=[5,5]

#Pre-processing 
train_set=train_set.astype('float32')
train_set-=np.mean(train_set)
train_set/=np.max(train_set)
#Define the (beast) model
model = Sequential()
model.add( Conv3D(32, (nb_conv[0], nb_conv[0], nb_conv[0]), 
                  activation="relu", 
                  weights=None, 
                  activity_regularizer=None, 
                  strides=(1, 1, 1), 
                  padding="valid", 
                  data_format="channels_first", 
                  kernel_initializer="glorot_uniform", 
                  kernel_regularizer=None, 
                  bias_regularizer=None, 
                  kernel_constraint=None, 
                  bias_constraint=None, 
                  use_bias=True) )
#model.add(Convolution3D(nb_filters[0],
#                        nb_depth=nb_conv[0],
#                        nb_row=nb_conv[0],
#                        nb_col=nb_conv[0], 
#                        border_mode='full',
#                        input_shape=(1, img_rows,img_cols,patch_size), 
#                        activation='relu'))
model.add(MaxPooling3D(pool_size=(nb_pool[0],nb_pool[0],nb_pool[0])))
model.add(Dropout(0.5))
model.add(Flatten())
#model.add(Dense(128, init='normal', activation='relu'))
model.add(Dense(128, activation='relu', kernel_initializer='normal'))
model.add(Dropout(0.5))
#model.add(Dense(nb_classes,init='normal'))
model.add(Dense(nb_classes, kernel_initializer='normal'))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy', optimizer='RMSprop', metrics=['accuracy'])
#keras.utils.plot_model(model, to_file='model.png')

X_train_new,X_val_new,y_train_new,y_val_new= train_test_split(train_set, Y_train, test_size=0.2, random_state=4)
#Train the model
print('X_train_new shape',X_train_new.shape)
print('y_train_new shape',y_train_new.shape)
hist = model.fit(X_train_new,y_train_new,validation_data=(X_val_new,y_val_new),batch_size=batch_size,epochs=nb_epoch, shuffle=True)
print(model.summary())

model.save('3dcnn_deception_model.h5')
model=load_model('3dcnn_deception_model.h5')
score = model.evaluate(X_val_new,y_val_new,batch_size=batch_size)

print('Test score:', score[0])
print('Accuracy:',score[1])
predictions=model.predict(X_val_new)
predictions=np.array(predictions[0:10])
y_val_new=np.array(y_val_new[0:10])

print('1st 10 predictions',predictions)
print('1st 10 truth labels',y_val_new)
