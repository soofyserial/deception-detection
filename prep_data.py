import numpy as np
import pandas as pd
from models import *
import cv2
import keras
from datetime import datetime
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras import layers, models
from keras.layers.merge import concatenate
from keras.layers import Dense, Input, Flatten, Dropout, Add, Concatenate
from keras.layers import Conv1D, MaxPooling1D, Embedding, GlobalMaxPooling1D
from keras.layers import LSTM, Bidirectional
from keras.layers import Activation, BatchNormalization, SpatialDropout1D, GaussianNoise
from keras.models import Model, Sequential, load_model
from keras.layers.convolutional import Convolution3D, MaxPooling2D,MaxPooling3D, Conv3D, Conv2D
from keras.optimizers import Adam, Adadelta
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint, TerminateOnNaN
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.utils import np_utils, generic_utils

import gensim
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess
from gensim.models.keyedvectors import KeyedVectors

import librosa
import librosa.display as ld 
import pandas as pd
import glob
import speechpy
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import os

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

# Image Spec
#img_rows,img_cols,img_depth=120,120,20
#img_rows,img_cols,img_depth=120,120,20
##Gives 92% test accuracy
#img_rows,img_cols,img_depth=150,150,10
######################################
img_rows,img_cols,img_depth=200,200,5
batch_size=2
num_classes=2
# Training Data
X_tr=[]
X_tr_true=[]
X_tr_false=[]
nb_classes=2
nb_epochs=50
nb_batches=30
#True/False Classes
def get_vid_data():
    truelisting=os.listdir('./uni-mich-data/Clips/Truthful/')
    lielisting=os.listdir('./uni-mich-data/Clips/Deceptive/')
    path ='./models/'
    for vid in truelisting:
        vid = "./uni-mich-data/Clips/Truthful/"+vid
        print(vid)
        frames=[]
        cap = cv2.VideoCapture(vid)
        fps = cap.get(5)
        print ("Frames per seconds using video.get(cv2.cv.CV_CAP_PROP_FPS):{0}".format(fps))
    
        for k in range(20):
            ret, frame=cap.read()
        if frame is not None:
            frame=cv2.resize(frame,(img_rows,img_cols),interpolation=cv2.INTER_AREA)
            gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            frames.append(gray)

            #plt.imshow(gray,cmap=plt.get_cmap('gray'))
            #plt.xticks([]),plt.yticks([])
            #plt.show()
            #cv2.imshow('frame',gray)
        
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            cap.release()
            cv2.destroyAllWindows()
    
            input=np.array(frames)
            print (input.shape)
            ipt=np.rollaxis(np.rollaxis(input,2,0),2,0)
            print(ipt.shape)
    
            X_tr.append(ipt)
            X_tr_true.append(ipt)
    
    for vid in lielisting:
        vid = "./uni-mich-data/Clips/Deceptive/"+vid
        frames=[]
        cap = cv2.VideoCapture(vid)
        fps = cap.get(5)
        print ("Frames per seconds using video.get(cv2.cv.CV_CAP_PROP_FPS):{0}".format(fps))
    
        for k in range(20):
            ret, frame=cap.read()
            if frame is not None:
                frame=cv2.resize(frame,(img_rows,img_cols),interpolation=cv2.INTER_AREA)
                gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
                frames.append(gray)
        
                #plt.imshow(gray,cmap=plt.get_cmap('gray'))
                #plt.xticks([]),plt.yticks([])
                #plt.show()
                #cv2.imshow('frame',gray)
    
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                cap.release()
                cv2.destroyAllWindows()
                input=np.array(frames)
                print (input.shape)
                ipt=np.rollaxis(np.rollaxis(input,2,0),2,0)
                print(ipt.shape)
    
                X_tr.append(ipt)
                X_tr_false.append(ipt)

    X_tr_true_array = np.array(X_tr_true)
    num_truesamples = len(X_tr_true_array)
    print ('number of true samples',num_truesamples)

    X_tr_false_array = np.array(X_tr_false)
    num_falsesamples = len(X_tr_false_array)
    print ('number of false samples', num_falsesamples)

    X_tr_array = np.array(X_tr)
    num_samples=len(X_tr_array)
    print (num_samples)

    #Assign label to each class
    label = np.ones((num_samples),dtype=int)
    label[0:59]=1
    label[60:121]=0

    train_data=[X_tr_array,label]
    (X_train, y_train)=(train_data[0],train_data[1])
    print('X_train shape:', X_train.shape)
    print('y_train shape:',y_train.shape)

    train_set = np.zeros((num_samples,1,img_rows, img_cols, img_depth))
    for h in range(num_samples):
        train_set[h][0][:][:][:]=X_train[h,:,:,:]

    train_set_new = np.zeros((num_samples, 1, img_rows, img_cols, img_depth))
    for h in range(num_samples):
        train_set_new[h][0][:][:][:]=X_train[h,:,:,:]

    patch_size=20 #img depth or number of frames used for each video

    print(train_set.shape, 'train samples')
    #convert class vectors to binary class matrices
    vY_train=np_utils.to_categorical(y_train,nb_classes)

    #Pre-processing 
    train_set=train_set.astype('float32')
    train_set-=np.mean(train_set)
    train_set/=np.max(train_set)
    print('train set',train_set)
    V_X_train,V_X_val,V_y_train,V_y_val= train_test_split(train_set, vY_train, test_size=0.0, shuffle=False, stratify=None)
    return V_X_train, V_X_val, V_y_train, V_y_val
######################################### Audio ##########################
def get_audio_data():
    a_true_path = os.listdir('./uni-mich-data/Audio/Truthful/')
    a_false_path = os.listdir('./uni-mich-data/Audio/Deceptive/')
    #sample_length=500
    sample_length=1500
    mfcc_vec=[]
    def pad(array, offsets):
        result = np.zeros((20,sample_length))
        insertHere = [slice(offset[dim], offset[dim] + array.shape[dim]) for dim in range(array.ndim)]
        result[insertHere]=array
        return result

    def wav2mfcc(file_path, max_len):
        wave, sr = librosa.load(file_path, mono=True, sr=None)
        wave=wave[::3]
        mfcc = librosa.feature.mfcc(wave, sr=16000)
        mfcc = speechpy.processing.cmvnw(mfcc, win_size=301, variance_normalization=True)
        # If max length exceeds mfcc lengths then pad the remaining ones
        if(max_len > mfcc.shape[1]):
            pad_width = max_len - mfcc.shape[1]
            mfcc = np.pad(mfcc, pad_width=((0,0), (0, pad_width)), mode='constant')
    
        # Else cutoff the remaining parts 
        else:
            mfcc=mfcc[:,:max_len]
   
        return mfcc

    for wav in a_true_path:
        wavefile = './uni-mich-data/Audio/Truthful/'+wav
        mfcc = wav2mfcc(wavefile,sample_length)
        #print(mfcc)
        print('mfcc shape',mfcc.shape)
        mfcc_vec.append(mfcc)
    
    for wav in a_false_path:
        wavefile = './uni-mich-data/Audio/Deceptive/'+wav
        mfcc = wav2mfcc(wavefile,sample_length)
        #print(mfcc)
        print('mfcc shape',mfcc.shape)
        mfcc_vec.append(mfcc)

    mfcc_vec_array = np.array(mfcc_vec)
    print('finishing appending mfcc vectors')
    num_samples=len(mfcc_vec_array)
    print('num samples',num_samples)
    #Assign Label to each class
    label=np.ones((num_samples,),dtype=int)
    label[0:59]=1
    label[60:121]=0
    train_data = [mfcc_vec_array,label]
    (X_train, y_train)=(train_data[0], train_data[1])
    print('X_train shape:', X_train.shape)
    print('y_train shape:', y_train.shape)
    # Feature dimension
    feature_dim_1 = mfcc.shape[0]
    feature_dim_2 = mfcc.shape[1]
    achannel =3
    nb_epoch=25
    batch_size=2
    verbose=1
    num_classes=2

    train_set=np.zeros((num_samples,1, feature_dim_1, feature_dim_2))

    for h in range(num_samples):
        train_set[h][0][:][:]=X_train[h,:,:]

    print(train_set.shape, 'train samples')

    aY_train = np_utils.to_categorical(y_train, num_classes)

    A_X_train, A_X_val, A_y_train, A_y_val = train_test_split(train_set, aY_train, test_size=0.0, shuffle=False, stratify=None)
    return A_X_train, A_X_val, A_y_train, A_y_val, feature_dim_1, feature_dim_2
#################################Word data ##############################

def get_word_data():
    w_true_path=os.listdir('./uni-mich-data/Transcription/Truthful/')
    w_false_path=os.listdir('./uni-mich-data/Transcription/Deceptive/')
    train_data=[]
    for i in w_true_path:
        i = './uni-mich-data/Transcription/Truthful/'+i
        #print(i)
        data = pd.read_csv(i)
        train_data.append(data)

    for j in w_false_path:
        j = './uni-mich-data/Transcription/Deceptive/'+j
        data = pd.read_csv(j)
        train_data.append(data)

    train_data=np.array(train_data)
    #print(train_data.shape)
    num_samples= len(train_data)
    #print(num_samples)
    df = pd.DataFrame({'Data':train_data[:]})
    df['Data']=df['Data'].astype('str')
    #print(df)
    #print(df.shape)
    labels = np.ones((num_samples), dtype=int)
    labels[0:59]=1
    labels[60:121]=0
    df_labels = pd.DataFrame({'Label':labels[:]})
    #print (df_labels)
    df['Label']=df_labels
    print(df)

    wY_train=to_categorical(np.asarray(df.Label))
    print(wY_train)
    #val_data=df.sample(frac=0.205,random_state=200)
    val_data=df.sample(frac=0.0,random_state=0)
    train_data=df.drop(val_data.index)

    text=train_data.Data
    print(text)

    NUM_WORDS=20000
    tokenizer=Tokenizer(num_words=NUM_WORDS,filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n\'')
    tokenizer.fit_on_texts(text)
    sequences_train=tokenizer.texts_to_sequences(text)
    sequences_valid=tokenizer.texts_to_sequences(val_data.Data)
    word_index=tokenizer.word_index
    print('Found %s unique tokens.' %len(word_index))

    W_X_train=pad_sequences(sequences_train)
    W_X_val=pad_sequences(sequences_valid,maxlen=W_X_train.shape[1])
    W_y_train=to_categorical(np.asarray(train_data.Label))
    print('Shape of X train tensor:', W_X_train.shape)

    maxlen=W_X_train.shape[1]
    print('maxlen',maxlen)
    word_vectors = KeyedVectors.load_word2vec_format('./Word2Vec/GoogleNews-vectors-negative300.bin', binary=True)

    EMBEDDING_DIM=300
    vocabulary_size=min(len(word_index)+1, NUM_WORDS)
    embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
    for word, i in word_index.items():
        if i>=NUM_WORDS:
            continue
        try:
            embedding_vector=word_vectors[word]
            embedding_matrix[i]=embedding_vector
        except KeyError:
            embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
        
    del(word_vectors)

    from keras.layers import Embedding
    embedding_layer = Embedding(vocabulary_size,
                           EMBEDDING_DIM,
                           weights=[embedding_matrix],
                           trainable=True)

    print(embedding_vector.shape)

    conv_filters = 128 
    weight_vec = list(np.max(np.sum(W_y_train, axis=0))/np.sum(W_y_train, axis=0))
    class_weight = {i: weight_vec[i] for i in range (2)}
    print(weight_vec)
    print(class_weight)
    return W_X_train, W_X_val, W_y_train, maxlen, word_index, embedding_matrix

################################ Microfeatures data #################################
def get_mf_data():
    mf='./uni-mich-data/Annotation/all_gestures.csv'
    data = pd.read_csv(mf)
    data = data.drop(['id'],axis=1)
    print(data.head())
    #print(data.shape)
    train_data=np.array(data)
    num_samples=len(train_data)
    print(num_samples)
    labels = np.ones((num_samples), dtype=int)
    labels[0:59]=1
    labels[60:121]=0
    data_labels=pd.DataFrame({'Label':labels[:]})
    print(data_labels)
    data['Label']=data_labels
    data=data.drop(['class'],axis=1)
    mf=data.drop(['Label'],axis=1)
    print(data.head())
    M_X_train = np.array(mf)
    M_y_train = to_categorical(np.asarray(data.Label))
    print('Shape of M_X_train:', M_X_train.shape)
    print('Shape of M_y_train:', M_y_train.shape)
    return M_X_train, M_y_train
