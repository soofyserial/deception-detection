import numpy as np
import pandas as pd
from models import *
from prep_data import *
import cv2
import keras
from datetime import datetime
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras import layers, models
from keras.layers.merge import concatenate
from keras.layers import Dense, Input, Flatten, Dropout, Add, Concatenate
from keras.layers import Conv1D, MaxPooling1D, Embedding, GlobalMaxPooling1D
from keras.layers import LSTM, Bidirectional
from keras.layers import Activation, BatchNormalization, SpatialDropout1D, GaussianNoise
from keras.models import Model, Sequential, load_model
from keras.layers.convolutional import Convolution3D, MaxPooling2D,MaxPooling3D, Conv3D, Conv2D
from keras.optimizers import Adam, Adadelta
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint, TerminateOnNaN
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.utils import np_utils, generic_utils, plot_model

import gensim
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess
from gensim.models.keyedvectors import KeyedVectors

import librosa
import librosa.display as ld 
import glob
import speechpy
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import tensorflow as tf
import os

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
#from sklearn import cross_validation
from sklearn import preprocessing

LOG_PATH_BASE='logs/'
fig_path='figs/'
#### Video Data ####


# Image Spec
#img_rows,img_cols,img_depth=200,200,20
#img_rows,img_cols,img_depth=120,120,20
##Gives 92% test accuracy
#img_rows,img_cols,img_depth=150,150,10
######################################
img_rows,img_cols,img_depth=200,200,5
batch_size=2
num_classes=2
# Training Data
X_tr=[]
X_tr_true=[]
X_tr_false=[]
nb_classes=2
nb_epochs=50
nb_batches=30
EMBEDDING_DIM=300
path ='./models/'
def pred_label(x):
    index_max = np.argmax(x)
    if index_max==0:
        label='tru'
    else:
        if index_max==1:
            label='lie'
    return label
#weight_vec = list(np.max(np.sum(W_y_train, axis=0))/np.sum(W_y_train, axis=0))
#class_weight = {i: weight_vec[i] for i in range (2)}
#print(weight_vec)
#print(class_weight)
#print('are V_y_train and A_y_train identical?',np.array_equal(vY_train,aY_train))
#print('are V_y_train and W_y_train identical?',np.array_equal(vY_train,wY_train))
#print('are A_y_train and W_y_train identical?',np.array_equal(aY_train,wY_train))

#### Create Train and Validation Split ####
#V_X_train, V_X_val, V_y_train, V_y_val = get_vid_data()
#A_X_train, A_X_val, A_y_train, A_y_val, feature_dim_1, feature_dim_2 = get_audio_data()
#W_X_train, W_X_val, W_y_train, maxlen, word_index, embedding_matrix = get_word_data()
#M_X_train, M_y_train=get_mf_data()
#y_train=V_y_train
#V_X_train_new, V_X_val_new, A_X_train_new, A_X_val_new, W_X_train_new, W_X_val_new, M_X_train_new, M_X_val_new, y_train, y_valid=train_test_split(V_X_train, A_X_train, W_X_train, M_X_train, y_train, random_state=4, test_size=0.2) 

############################Multiple Input Single Output Model with Microfeatures###################################
def miso_mf(save, load, train, pred, plot):
    print('STARTING MULTI INPUT SINGLE OUTPUT MODEL WITH MICROFEATURES')
    V_X_train, V_X_val, V_y_train, V_y_val = get_vid_data()
    A_X_train, A_X_val, A_y_train, A_y_val, feature_dim_1, feature_dim_2 = get_audio_data()
    W_X_train, W_X_val, W_y_train, maxlen, word_index, embedding_matrix = get_word_data()
    M_X_train, M_y_train=get_mf_data()
    y_train=V_y_train
    #V_X_train_new, V_X_val_new, A_X_train_new, A_X_val_new, W_X_train_new, W_X_val_new, y_train, y_valid=train_test_split(V_X_train, A_X_train, W_X_train, y_train, random_state=4, test_size=0.2) 
    V_X_train_new, V_X_val_new, A_X_train_new, A_X_val_new, W_X_train_new, W_X_val_new, M_X_train_new, M_X_val_new, y_train, y_valid=train_test_split(V_X_train, A_X_train, W_X_train, M_X_train, y_train, random_state=4, test_size=0.2) 
    mf_vec_length=M_X_train.shape[1]
    oc_model_mf = one_class_combined_model_with_micro(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen,word_index,EMBEDDING_DIM,embedding_matrix, mf_vec_length)
    mycallbacks = callback_creator(oc_model_mf, 5, 'val_acc')
    if(train):
        hist=oc_model_mf.fit([V_X_train_new, A_X_train_new, W_X_train_new, M_X_train_new], [y_train], batch_size=nb_batches, epochs=nb_epochs, verbose=1, validation_data=([V_X_val_new, A_X_val_new, W_X_val_new, M_X_val_new],y_valid), callbacks=mycallbacks)
        print('multi-modal including microfeatures CNNs with MLP training done.... saving model')
    if(save):
        oc_model_mf.save(path+'final_combined_with_mf_deception_model.h5')
        print('model saved!')
    if(load):
        oc_model_mf = load_model(path+'final_combined_with_mf_deception_model.h5')
    if(plot):
        plot_model(oc_model_mf, to_file=fig_path+'oc_model_mf.png')
    if(pred):
        preds = oc_model_mf.predict([V_X_val_new, A_X_val_new, W_X_val_new, M_X_val_new])
        print(np.apply_along_axis(pred_label, axis=1, arr=preds))
        print(np.apply_along_axis(pred_label, axis=1, arr=y_valid))
############################Multiple Input Single Output Model###################################

def miso(save, load, train, pred, plot):
    print('STARTING MULTI INPUT SINGLE OUTPUT MODEL NO MICROFEATURES')
    V_X_train, V_X_val, V_y_train, V_y_val = get_vid_data()
    A_X_train, A_X_val, A_y_train, A_y_val, feature_dim_1, feature_dim_2 = get_audio_data()
    W_X_train, W_X_val, W_y_train, maxlen, word_index, embedding_matrix = get_word_data()
    y_train=V_y_train
    V_X_train_new, V_X_val_new, A_X_train_new, A_X_val_new, W_X_train_new, W_X_val_new, y_train, y_valid=train_test_split(V_X_train, A_X_train, W_X_train, y_train, random_state=4, test_size=0.2) 
    oc_model = one_class_combined_model(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen,word_index,EMBEDDING_DIM,embedding_matrix)
    mycallbacks = callback_creator(oc_model, 5, 'val_acc')
    if(train):
        hist=oc_model.fit([V_X_train_new, A_X_train_new, W_X_train_new], [y_train], batch_size=nb_batches, epochs=nb_epochs, verbose=1, validation_data=([V_X_val_new, A_X_val_new, W_X_val_new],y_valid), callbacks=mycallbacks)
        print('multi-modal CNNs with MLP training done')
    if(save):
        print('saving model')
        oc_model.save(path+'final_combined_deception_model.h5')
        print('model saved!')
    if(load):
        oc_model = load_model(path+'final_combined_deception_model.h5')
    if(plot):
        plot_model(oc_model, to_file=fig_path+'oc_model.png')
    if(pred):
        preds = oc_model.predict([V_X_val_new, A_X_val_new, W_X_val_new])
        print(np.apply_along_axis(pred_label, axis=1, arr=preds))
        print(np.apply_along_axis(pred_label, axis=1, arr=y_valid))
############################Multiple Input Multiple Output Model#################################
def mimo(save, load, train, plot):
    print('STARTING MULTI INPUT MULTIPLE OUTPUT MODEL NO MICROFEATURES')
    V_X_train, V_X_val, V_y_train, V_y_val = get_vid_data()
    A_X_train, A_X_val, A_y_train, A_y_val, feature_dim_1, feature_dim_2 = get_audio_data()
    W_X_train, W_X_val, W_y_train, maxlen, word_index, embedding_matrix = get_word_data()
    y_train=V_y_train
    V_X_train_new, V_X_val_new, A_X_train_new, A_X_val_new, W_X_train_new, W_X_val_new, y_train, y_valid=train_test_split(V_X_train, A_X_train, W_X_train, y_train, random_state=4, test_size=0.2) 
    c_model = combined_model(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen,word_index,EMBEDDING_DIM,embedding_matrix)
    mycallbacks = callback_creator(c_model,2, 'val_vid_classify_acc')
    if(train):
        hist=c_model.fit([V_X_train_new, A_X_train_new, W_X_train_new], [y_train,y_train,y_train], batch_size=nb_batches, epochs=nb_epochs, verbose=1, validation_data=([V_X_val_new, A_X_val_new, W_X_val_new],[y_valid,y_valid,y_valid]), callbacks=mycallbacks)
        print('multi-modal training done')
    if(save):
        print('saving model')
        c_model.save(path+'combined_deception_model.h5')
        print('model saved!')
