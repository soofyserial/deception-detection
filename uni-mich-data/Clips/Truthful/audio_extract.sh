for i in *.mp4;
do
    #ffmpeg -i "$i" -vn -acodec copy "${i%.mp4}_audio.wav";
    ffmpeg -i "$i" -map 0:a "${i%.mp4}_audio.wav" -map 0:v "${i%.mp4}_onlyvideo.avi";
done
