
# coding: utf-8

# In[1]:


import os
import numpy as np
import librosa
import librosa.display as ld
import pandas as pd
import glob
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import speechpy
import keras
import keras.models
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.layers.noise import GaussianNoise
from keras.utils import to_categorical, np_utils, generic_utils
from keras.optimizers import SGD
from keras import backend as K
from sklearn.model_selection import train_test_split
from sklearn import cross_validation
from sklearn import preprocessing 

#path = os.listdir('./uni-mich-data/Audio/Truthful/')
true_path = os.listdir('./uni-mich-data/Audio/Truthful/')
false_path = os.listdir('./uni-mich-data/Audio/Deceptive/')
sample_length=3000
mfcc_vec=[]
opt = SGD(lr=0.001)
def pad(array, offsets):
    result = np.zeros((20,sample_length))
    insertHere = [slice(offset[dim], offset[dim] + array.shape[dim]) for dim in range(array.ndim)]
    result[insertHere]=array
    return result

def wav2mfcc(file_path, max_len):
    wave, sr = librosa.load(file_path, mono=True, sr=None)
    wave=wave[::3]
    mfcc = librosa.feature.mfcc(wave, sr=16000)
    mfcc = speechpy.processing.cmvnw(mfcc, win_size=301, variance_normalization=True)
    # If max length exceeds mfcc lengths then pad the remaining ones
    if(max_len > mfcc.shape[1]):
        pad_width = max_len - mfcc.shape[1]
        mfcc = np.pad(mfcc, pad_width=((0,0), (0, pad_width)), mode='constant')
    
    # Else cutoff the remaining parts 
    else:
        mfcc=mfcc[:,:max_len]
   
    return mfcc

for wav in true_path:
    wavefile = './uni-mich-data/Audio/Truthful/'+wav
    mfcc = wav2mfcc(wavefile,sample_length)
    #print(mfcc)
    #print(mfcc.shape)
    mfcc_vec.append(mfcc)
    
for wav in false_path:
    wavefile = './uni-mich-data/Audio/Deceptive/'+wav
    mfcc = wav2mfcc(wavefile,sample_length)
    #print(mfcc)
    #print(mfcc.shape)
    mfcc_vec.append(mfcc)

mfcc_vec_array = np.array(mfcc_vec)
print('finishing appending mfcc vectors')
#print(len(mfcc_vec_array))
num_samples=len(mfcc_vec_array)
#Assign Label to each class
label=np.ones((num_samples,),dtype=int)
label[0:49]=1
label[50:108]=0

train_data = [mfcc_vec_array,label]
(X_train, y_train)=(train_data[0], train_data[1])
print('X_train shape:', X_train.shape)
print('y_train shape:', y_train.shape)
# Feature dimension
feature_dim_1 = mfcc.shape[0]
feature_dim_2 = mfcc.shape[1]
channel =3
epochs=25
batch_size=2
verbose=1
num_classes=2

#train_set=np.zeros((num_samples, 1, feature_dim_1, feature_dim_2))
#for h in range(num_samples):
#    train_set[h][0][:][:]=X_train[h,:,:]
#print (train_set.shape, 'train samples')
#train_set=train_set.astype('float32')
#train_set-=np.mean(train_set)
#train_set/=np.max(train_set)
X_train, X_test, y_train, y_test=train_test_split(X_train, y_train, test_size=0.2, random_state=4)
y_train = to_categorical(y_train)
y_test=to_categorical(y_test)
X_train = X_train.reshape(X_train.shape[0], feature_dim_1, feature_dim_2, 1)
X_test= X_test.reshape(X_test.shape[0],feature_dim_1, feature_dim_2,1)
#X_train_new, X_test_new, y_train_new, y_test_new=train_test_split(X_train, y_train, test_size=0.2, random_state=4)
print('finishing splitting training and test datasets')
print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)
#X_train_new = X_train_new.reshape(X_train.shape[0], feature_dim_1, feature_dim_2, channel)
#X_test_new = X_test_new.reshape(X_test_new.shape[0], feature_dim_1, feature_dim_2, channel)

#y_train_new=to_categorical(y_train_new)
#y_test_new=to_categorical(y_test_new)


# In[2]:


def get_model():
    model = Sequential()
    model.add(Conv2D(32,(2,2), 
                     activation="relu",
                     input_shape=(feature_dim_1, feature_dim_2, 1)
                    #weights=None,
                    #activity_regularizer=None,
                    #strides=(1,1),
                    #padding="valid",
                    #kernel_initializer="glorot_uniform"
                    ))
    model.add(GaussianNoise(0.1))
    model.add(Conv2D(48, kernel_size=(2,2), activation="relu"))
    model.add(Conv2D(120, kernel_size=(2,2), activation="relu"))    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation="relu"))
    model.add(Dropout(0.25))
    #model.add(Dense(64, activation="relu"))
    model.add(Dense(300, activation="relu"))
    model.add(Dropout(0.4))
    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss=keras.losses.categorical_crossentropy, 
                 optimizer=keras.optimizers.Adadelta(lr=0.01),
                 metrics=['accuracy'])
    return model


# In[ ]:


#print(y_train_new)
#print(y_train_new.shape)
model = get_model()
print(model.summary())
get_feature_vector =K.function([model.layers[0].input],[model.layers[9].output])
feature_vector = get_feature_vector([X_train])[0]
print(feature_vector.shape)

def vis_feature(data):
    plt.figure(1, figsize=(25,25))
    features = data
    plt.imshow(features)
    plt.show()

vis_feature(feature_vector)

model.fit(X_train, y_train,batch_size=batch_size, epochs=epochs, verbose=verbose, validation_data=(X_test, y_test))
model.save('3dcnn_deception_audio_model.h5')

# In[ ]:


#import tensorflow as tf
#print(tf.__version__)
#print(keras.__version__)
