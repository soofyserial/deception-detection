import numpy as np
import pandas as pd

import cv2
import keras
from datetime import datetime
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras import layers, models
from keras.layers.merge import concatenate
from keras.layers import Dense, Input, Flatten, Dropout, Add, Concatenate, multiply
from keras.layers import Conv1D, MaxPooling1D, Embedding, GlobalMaxPooling1D
from keras.layers import LSTM, Bidirectional
from keras.layers import Activation, BatchNormalization, SpatialDropout1D, GaussianNoise
from keras.models import Model, Sequential, load_model
from keras.layers.convolutional import Convolution3D, MaxPooling2D,MaxPooling3D, Conv3D, Conv2D
from keras.optimizers import Adam, Adadelta
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint, TerminateOnNaN
from keras import backend as K
from keras import regularizers
import tensorflow as tf
K.set_image_dim_ordering('th')
from keras.utils import np_utils, generic_utils
nb_classes=2
nb_epochs=50
nb_batches=30
num_classes=2
conv_filters = 128 
LOG_PATH_BASE='logs/'
#########################################################################
#def MLP(feature_vec_1, feature_vec_2, feature_vec_3):
def MLP():
    inp1=Input(shape=(300,))
    inp2=Input(shape=(300,))
    inp3=Input(shape=(300,))
    merged = concatenate([inp1,inp2,inp3],axis=1)
  
    layer = Dense(1024, activation='relu')(merged)
    drp1=Dropout(0.5)(layer)
    out=Dense(num_classes, activation='softmax')(drp1)
    model = Model(inputs=[inp1,inp2,inp3],outputs=[out])
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    adadelta = Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
    model.compile(optimizer=adadelta, loss='categorical_crossentropy',metrics=['accuracy'])
    print(model.summary())
    return model
    
##########################################################################
def combined_model(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen, word_index, EMBEDDING_DIM, embedding_matrix):
    #inp1=Input(shape=(1,img_rows, img_cols, img_depth), name='inp1')
    #v = Conv3D(32, kernel_size=(3,3,3), strides=(1,1,1))(inp1)
    #v = MaxPooling3D(pool_size=(3,3,3))(v)
    #v = Dropout(0.5)(v)
    #flt_v = Flatten()(v)
    #v = Dense(300, activation='relu', name='vid_dense300')(flt_v)
    #dns1_v = Dense(128, activation='relu', kernel_initializer='normal')(flt_v)
    #drp_v = Dropout(0.5)(dns1_v)
    #out1 = Dense(num_classes, activation='softmax', name='vid_classify')(drp_v)
    #out1=Dense(num_classes, activation='softmax', name='vid_classify')(v)

    inp1=Input(shape=(1,img_rows, img_cols, img_depth), name='inp1')
    gn1_v = GaussianNoise(0.3)(inp1)
    v = Conv3D(32, kernel_size=(3,3,3), strides=(1,1,1), activation='relu', padding='same')(gn1_v)
    v = MaxPooling3D(pool_size=(3,3,3))(v)
    v = Dropout(0.5)(v)
    v = Conv3D(48, kernel_size=(3,3,3), strides=(1,1,1), activation='relu', padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    v = Conv3D(64, kernel_size=(3,3,3), strides=(1,1,1), activation='relu',padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    flt_v = Flatten()(v)
    dns1_v = Dense(128, activation='relu', kernel_initializer='normal')(flt_v)
    drp_v = Dropout(0.5)(dns1_v)
    v = Dense(300, activation='relu', name='vid_dense300')(drp_v)
    out1 = Dense(num_classes, activation='softmax',name='vid_classify')(flt_v)


    inp2 = Input(shape=(1,feature_dim_1, feature_dim_2), name='inp2')
    a = Conv2D(32, kernel_size=(2,2), activation='relu', padding='same')(inp2)
    a = Conv2D(48, kernel_size=(2,2), activation='relu')(a)
    a = Conv2D(120, kernel_size=(2,2), activation='relu')(a)
    a = MaxPooling2D(pool_size=(2,2))(a)
    a = Dropout(0.25)(a)
    flt_a = Flatten()(a)
    dns1_a = Dense(128, activation='relu')(flt_a)
    drp_a = Dropout(0.25)(dns1_a)
    
    a = Dense(300, activation='relu', name='audio_dense300')(flt_a)
    out2=Dense(num_classes, activation='softmax', name='aud_classify')(drp_a)

    inp3= Input(shape=(maxlen,), dtype='int64', name='inp3')
    emb = Embedding(len(word_index)+1, EMBEDDING_DIM, weights=[embedding_matrix])(inp3)

    #Specify each convolution layer and their kernel size i.e. n-grams
    conv1_1 = Conv1D(filters=conv_filters, kernel_size=3)(emb)
    actv1_1 = Activation('relu')(conv1_1)
    btch1_1 = BatchNormalization()(actv1_1)
    drp1_1 = Dropout(0.2)(btch1_1)
    glmp1_1 = GlobalMaxPooling1D()(drp1_1)

    conv1_2 = Conv1D(filters=conv_filters, kernel_size=4)(emb)
    actv1_2 = Activation('relu')(conv1_2)
    btch1_2 = BatchNormalization()(actv1_2)
    drp1_2 = Dropout(0.2)(btch1_2)
    glmp1_2 = GlobalMaxPooling1D()(drp1_2)

    conv1_3 = Conv1D(filters=conv_filters, kernel_size=5)(emb)
    actv1_3 = Activation('relu')(conv1_3)
    btch1_3 = BatchNormalization()(actv1_3)
    drp1_3 = Dropout(0.2)(btch1_3)
    glmp1_3 = GlobalMaxPooling1D()(drp1_3)

    conv1_4 = Conv1D(filters=conv_filters, kernel_size=6)(emb)
    actv1_4 = Activation('relu')(conv1_4)
    btch1_4 = BatchNormalization()(actv1_4)
    drp1_4 = Dropout(0.2)(btch1_4)
    glmp1_4 = GlobalMaxPooling1D()(drp1_4)

    # Gather all convolution layers
    cnct = concatenate([glmp1_1, glmp1_2, glmp1_3, glmp1_4], axis =1)
    drp1 = Dropout(0.2)(cnct)

    w = Dense(300, activation='relu', name='word_dense300')(drp1)
    #out3=Dense(num_classes, activation='softmax', name='txt_classify')(w)
    dns1 = Dense(32, activation='relu')(drp1)
    btch1 = BatchNormalization()(dns1)
    drp2 = Dropout(0.2)(btch1)
    out3=Dense(num_classes, activation='sigmoid', name='txt_classify')(drp2)
    merged = concatenate([v,a,w],axis=1)
    #out=Dense(1024, activation='relu')(merged)
    #out=Dropout(0.5)(out)
    #out=Dense(num_classes, activation='softmax')(out)


    model = Model(inputs=[inp1,inp2,inp3], outputs=[out1,out2,out3])
    adam = Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='categorical_crossentropy',metrics=['accuracy'])
    print(model.summary())
    return model
##############################################################
def one_class_combined_model(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen, word_index, EMBEDDING_DIM, embedding_matrix):

    #############################Video#############################
    inp1=Input(shape=(1,img_rows, img_cols, img_depth), name='inp1')
    gn1_v = GaussianNoise(0.3)(inp1)
    v = Conv3D(32, kernel_size=(5,5,5), strides=(1,1,1), activation='relu', padding='same')(gn1_v)
    v = MaxPooling3D(pool_size=(3,3,3))(v)
    v = Dropout(0.5)(v)
    v = Conv3D(48, kernel_size=(5,5,5), strides=(1,1,1), activation='relu', padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    v = Conv3D(64, kernel_size=(5,5,5), strides=(1,1,1), activation='relu',padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    flt_v = Flatten()(v)
    dns1_v = Dense(128, activation='relu', kernel_initializer='normal')(flt_v)
    drp_v = Dropout(0.5)(dns1_v)
    v = Dense(300, activation='relu', name='vid_dense300')(drp_v)

    #############################Audio#############################
    inp2 = Input(shape=(1,feature_dim_1, feature_dim_2), name='inp2')
    gn1_a = GaussianNoise(0.3)(inp2)
    a = Conv2D(32, kernel_size=(2,2), activation='relu', padding='same')(gn1_a)
    mx1_a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Conv2D(48, kernel_size=(2,2), activation='relu', padding='same')(mx1_a)
    mx2_a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Conv2D(120, kernel_size=(2,2), activation='relu',padding='same')(a)
    a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Dropout(0.25)(a)
    flt_a = Flatten()(a)
    dns1_a = Dense(128, activation='relu')(flt_a)
    drp_a = Dropout(0.25)(dns1_a)   
    a = Dense(300, activation='relu', name='audio_dense300')(drp_a)

    #############################Text#############################
    inp3= Input(shape=(maxlen,), dtype='int64', name='inp3')
    emb = Embedding(len(word_index)+1, EMBEDDING_DIM, weights=[embedding_matrix])(inp3)

    #Specify each convolution layer and their kernel size i.e. n-grams
    conv1_1 = Conv1D(filters=conv_filters, kernel_size=3)(emb)
    actv1_1 = Activation('relu')(conv1_1)
    btch1_1 = BatchNormalization()(actv1_1)
    drp1_1 = Dropout(0.2)(btch1_1)
    glmp1_1 = GlobalMaxPooling1D()(drp1_1)

    conv1_2 = Conv1D(filters=conv_filters, kernel_size=4)(emb)
    actv1_2 = Activation('relu')(conv1_2)
    btch1_2 = BatchNormalization()(actv1_2)
    drp1_2 = Dropout(0.2)(btch1_2)
    glmp1_2 = GlobalMaxPooling1D()(drp1_2)

    conv1_3 = Conv1D(filters=conv_filters, kernel_size=5)(emb)
    actv1_3 = Activation('relu')(conv1_3)
    btch1_3 = BatchNormalization()(actv1_3)
    drp1_3 = Dropout(0.2)(btch1_3)
    glmp1_3 = GlobalMaxPooling1D()(drp1_3)

    conv1_4 = Conv1D(filters=conv_filters, kernel_size=6)(emb)
    actv1_4 = Activation('relu')(conv1_4)
    btch1_4 = BatchNormalization()(actv1_4)
    drp1_4 = Dropout(0.2)(btch1_4)
    glmp1_4 = GlobalMaxPooling1D()(drp1_4)

    # Gather all convolution layers
    cnct = concatenate([glmp1_1, glmp1_2, glmp1_3, glmp1_4], axis =1)
    drp1 = Dropout(0.2)(cnct)
    dns1 = Dense(32, activation='relu')(drp1)
    btch1 = BatchNormalization()(dns1)
    drp2 = Dropout(0.2)(btch1)

    w = Dense(300, activation='relu', name='word_dense300')(drp2)
    merged = concatenate([v,a,w],axis=1)

    # Final MLP layer
    out=Dense(1024, activation='relu')(merged)
    out=Dropout(0.5)(out)
    out=Dense(num_classes, activation='softmax')(out)


    model = Model(inputs=[inp1,inp2,inp3], outputs=[out])
    adam = Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='categorical_crossentropy',metrics=['accuracy'])
    print(model.summary())
    return model
##############################################################
def one_class_combined_model_with_micro(img_rows, img_cols, img_depth, feature_dim_1, feature_dim_2,maxlen, word_index, EMBEDDING_DIM, embedding_matrix, microfeature_vec_length):

    #############################Video#############################
    inp1=Input(shape=(1,img_rows, img_cols, img_depth), name='inp1')
    gn1_v = GaussianNoise(0.3)(inp1)
    v = Conv3D(32, kernel_size=(5,5,5), strides=(1,1,1), activation='relu', padding='same')(gn1_v)
    v = MaxPooling3D(pool_size=(3,3,3))(v)
    v = Dropout(0.5)(v)
    v = Conv3D(48, kernel_size=(5,5,5), strides=(1,1,1), activation='relu', padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    v = Conv3D(64, kernel_size=(5,5,5), strides=(1,1,1), activation='relu',padding='same')(v)
    v = MaxPooling3D(pool_size=(3,3,3), padding='same')(v)
    v = Dropout(0.5)(v)
    flt_v = Flatten()(v)
    dns1_v = Dense(128, activation='relu', kernel_initializer='normal')(flt_v)
    drp_v = Dropout(0.5)(dns1_v)
    v = Dense(300, activation='relu', name='vid_dense300')(drp_v)

    #############################Audio#############################
    inp2 = Input(shape=(1,feature_dim_1, feature_dim_2), name='inp2')
    gn1_a = GaussianNoise(0.3)(inp2)
    a = Conv2D(32, kernel_size=(2,2), activation='relu', padding='same')(gn1_a)
    mx1_a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Conv2D(48, kernel_size=(2,2), activation='relu', padding='same')(mx1_a)
    mx2_a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Conv2D(120, kernel_size=(2,2), activation='relu',padding='same')(a)
    a = MaxPooling2D(pool_size=(2,2), padding='same')(a)
    a = Dropout(0.25)(a)
    flt_a = Flatten()(a)
    dns1_a = Dense(128, activation='relu')(flt_a)
    drp_a = Dropout(0.25)(dns1_a)   
    a = Dense(300, activation='relu', name='audio_dense300')(drp_a)

    #############################Text#############################
    inp3= Input(shape=(maxlen,), dtype='int64', name='inp3')
    emb = Embedding(len(word_index)+1, EMBEDDING_DIM, weights=[embedding_matrix])(inp3)

    #Specify each convolution layer and their kernel size i.e. n-grams
    conv1_1 = Conv1D(filters=conv_filters, kernel_size=3)(emb)
    actv1_1 = Activation('relu')(conv1_1)
    btch1_1 = BatchNormalization()(actv1_1)
    drp1_1 = Dropout(0.2)(btch1_1)
    glmp1_1 = GlobalMaxPooling1D()(drp1_1)

    conv1_2 = Conv1D(filters=conv_filters, kernel_size=4)(emb)
    actv1_2 = Activation('relu')(conv1_2)
    btch1_2 = BatchNormalization()(actv1_2)
    drp1_2 = Dropout(0.2)(btch1_2)
    glmp1_2 = GlobalMaxPooling1D()(drp1_2)

    conv1_3 = Conv1D(filters=conv_filters, kernel_size=5)(emb)
    actv1_3 = Activation('relu')(conv1_3)
    btch1_3 = BatchNormalization()(actv1_3)
    drp1_3 = Dropout(0.2)(btch1_3)
    glmp1_3 = GlobalMaxPooling1D()(drp1_3)

    conv1_4 = Conv1D(filters=conv_filters, kernel_size=6)(emb)
    actv1_4 = Activation('relu')(conv1_4)
    btch1_4 = BatchNormalization()(actv1_4)
    drp1_4 = Dropout(0.2)(btch1_4)
    glmp1_4 = GlobalMaxPooling1D()(drp1_4)

    # Gather all convolution layers
    cnct = concatenate([glmp1_1, glmp1_2, glmp1_3, glmp1_4], axis =1)
    drp1 = Dropout(0.2)(cnct)
    dns1 = Dense(32, activation='relu')(drp1)
    btch1 = BatchNormalization()(dns1)
    drp2 = Dropout(0.2)(btch1)

    w = Dense(300, activation='relu', name='word_dense300')(drp2)
    ###################### Micro features #########################
    inp4=Input(shape=(microfeature_vec_length,), name='inp4')
    m = inp4
    #m = tf.to_float(inp4)
    hadamard=multiply([v,a,w])
    #merged = concatenate([v,a,w,m],axis=1)
    merged=concatenate([hadamard,m],axis=1)
    # Final MLP layer
    out=Dense(1024, activation='relu')(merged)
    out=Dropout(0.5)(out)
    out=Dense(num_classes, activation='softmax')(out)


    model = Model(inputs=[inp1,inp2,inp3,inp4], outputs=[out])
    adam = Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='categorical_crossentropy',metrics=['accuracy'])
    print(model.summary())
    return model


def callback_creator(model, patience, metric):
    ts = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    log_path=LOG_PATH_BASE + ts + "_-_" + model.name
    tensorboard = TensorBoard(log_dir=log_path, write_graph=False, write_images=True, batch_size=nb_batches, write_grads=True)
    earlystopping = EarlyStopping(monitor=metric, min_delta=0, patience=patience, verbose=1, mode='auto')
    callbacks=[tensorboard,earlystopping,TerminateOnNaN()]
    return callbacks

