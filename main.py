from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from vid_audio_MLP import miso_mf, miso, mimo
#from models import *
#from prep_data import *

def get_parser():
    parser = ArgumentParser(description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-tr", "--train", dest="tr",action="store_true",default=True)
    parser.add_argument("-tst", "--test", dest="tst", action="store_true",default=False)
    parser.add_argument("-mo", "--multiple_output", dest="mo",action="store_true", default=False)
    parser.add_argument("-so", "--single_output", dest="so",action="store_true", default=False)
    parser.add_argument("-mf", "--microfeatures", dest="mf", action="store_true", default=False)
    parser.add_argument("-had", "--hadamard", dest="had", action="store_true", default=False)
    parser.add_argument("-save", "--save_model", dest="save", action="store_true", default=False)
    parser.add_argument("-load", "--load_model", dest="load", action="store_true", default=False)
    parser.add_argument("-plot", "--plot_model", dest="plot", action="store_true", default=False)
    return parser

def main(train, test, m_output, s_output, micro, hadamard, save, load, plot):
    if(micro and s_output and hadamard):
        miso_mf(save, load, train, test, plot)
    #if(s_output):
    #    miso(save, load, train, test, plot)
    #if(m_output):
    #    mimo(save, load, train, plot)
if __name__ == "__main__":
    args = get_parser().parse_args()
    print(args)
    main(args.tr,args.tst,args.mo, args.so, args.mf, args.had, args.save, args.load, args.plot)
